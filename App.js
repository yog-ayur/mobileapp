import React from 'react';
import AppNavigator from './src/navigation/AppNavigation';
import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from 'react-apollo';

const createApolloClient = () => {
  return new ApolloClient({
    link: new HttpLink({
      uri: 'https://yog-ayur.herokuapp.com/graphiql/',
    }),
    cache: new InMemoryCache(),
  });
};



export default function App() {
  const client = createApolloClient();
  return (
    <ApolloProvider client={client}>
      <AppNavigator />
    </ApolloProvider>
  );
}


