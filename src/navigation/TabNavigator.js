import React,{Component} from 'react';
import { Image, Dimensions } from 'react-native';
import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import Popular from '../components/Screens/TabScreens/Popular';
import Bonus from '../components/Screens/TabScreens/Bonus';
import Collections from '../components/Screens/TabScreens/Collections';
import Share from '../components/Screens/TabScreens/Share';
import Help from '../components/Screens/TabScreens/Help';



const height = Dimensions.get('window').height;


const TabNavigatorStack = createBottomTabNavigator({
    Popular: {
        screen: Popular,
    },
    Bonus: {
        screen: Bonus
    },
    Share: {
        screen: Share
    },
    Help: {
        screen: Help
    }
}, {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Popular') {
                    iconName = require('../../assets/star.png')
                } else if (routeName === 'Bonus') {
                    iconName = require('../../assets/bonus.png')
                } else if (routeName === 'Share') {
                    iconName = require('../../assets/share.png')
                } else if (routeName === 'Help') {
                    iconName = require('../../assets/help.png')
                }

                return <Image source={iconName} style={{ width: 24, height: 24 }} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: '#FFE598',
            inactiveTintColor: '#FFFFFF',
            style: {
                backgroundColor: '#A77248',
                borderTopLeftRadius: 16,
                borderTopRightRadius: 16,
                height: height * 9 / 100

            },
        },
    });

const TabNavigator = createAppContainer(TabNavigatorStack)

export default TabNavigator;

