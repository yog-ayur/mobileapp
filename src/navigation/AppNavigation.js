import { createStackNavigator, createAppContainer, createSwitchNavigator } from "react-navigation";
import Banner from '../components/Banner/Banner';
import SignIn from '../components/SignIn/SignIn';
import SignUp from '../components/SignIn/SignUp';
import SignIn2 from "../components/SignIn/SignIn2";
import ForgotPassword from "../components/SignIn/ForgotPasword";
import ConfirmPassword from "../components/SignIn/ConfirmPassword";
import Home from "../components/Home/Home";
import NewDeliveryAddress from "../components/Address/NewDeliveryAddress";
import Accordian from '../components/Accordian/Accordian';
import MyCart from "../components/Screens/DrawerScreens/MyCart";
import Product from '../components/Product/Product';
import TabNavigator from "./TabNavigator";

const RootStack = createStackNavigator({
    Banner: {
        screen: Banner
    },
    SignIn: {
        screen: SignIn
    },

    SignIn2: {
        screen: SignIn2
    },
    SignUp: {
        screen: SignUp
    },
    ForgotPassword: {
        screen: ForgotPassword
    },
    ConfirmPassword: {
        screen: ConfirmPassword
    },
    Home: {
        screen: Home
    },
    NewDeliveryAddress: {
        screen: NewDeliveryAddress
    },
    Accordian: {
        screen: Accordian
    },
    MyCart: {
        screen: MyCart
    },
    Product: {
        screen: Product
    },
    

}, {
    initialRouteName: 'Banner',
    headerMode: 'none'
})



const AppNavigator = createAppContainer(RootStack)

export default AppNavigator;