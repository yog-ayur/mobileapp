import { Dimensions } from 'react-native';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import MyProfile from '../components/Screens/DrawerScreens/MyProfile';
import MyOrders from '../components/Screens/DrawerScreens/MyOrders';
import MyCart from '../components/Screens/DrawerScreens/MyCart';
import MyEarnings from '../components/Screens/DrawerScreens/MyEarnings';
import WeeklyBonus from '../components/Screens/DrawerScreens/WeeklyBonus';
import ReferEarn from '../components/Screens/DrawerScreens/ReferEarn';
import Language from '../components/Screens/DrawerScreens/Language';
import Chat from '../components/Screens/DrawerScreens/Chat';
import SearchWholesellers from '../components/Screens/DrawerScreens/SearchWholesellers';
import SellerPannel from '../components/Screens/DrawerScreens/SellerPannel';
import CustomDrawer from './CustomDrawer';
import Popular from '../components/Screens/TabScreens/Popular';
import TabNavigator  from './TabNavigator';
import Product from '../components/Product/Product';
import SetDeliveryAddress from '../components/Screens/OrderScreens/SetDeliveryAddress';



const width = Dimensions.get('window').width;

const DrawerStack = createDrawerNavigator({
  Welcome: {
    screen: TabNavigator
  },
  MyProfile: {
    screen: MyProfile
  },
  MyOrders: {
    screen: MyOrders
  },
  MyCart: {
    screen: MyCart
  },
  MyEarnings: {
    screen: MyEarnings
  },
  WeeklyBonus: {
    screen: WeeklyBonus
  },
  ReferEarn: {
    screen: ReferEarn
  },
  SearchWholesellers: {
    screen: SearchWholesellers
  },
  SellerPannel: {
    screen: SellerPannel
  },
  Chat: {
    screen: Chat
  },
  Language: {
    screen: Language
  },
  Product:{
    screen:Product
  },
  SetDeliveryAddress:{
    screen:SetDeliveryAddress
  },
  Popular: {
    screen: Popular,
},

 
},
  {
    initialRouteName: 'Welcome',
    drawerWidth: width * 0.7,
    hideStatusBar: true,
    contentComponent: CustomDrawer,
    overlayColor: '#FFF2CB',
    contentOptions: {
      activeTintColor: '#A77248',
      activeBackgroundColor: '#FFF2CB',
    },
  }
);

const DrawerNavigator = createAppContainer(DrawerStack);

export default DrawerNavigator
