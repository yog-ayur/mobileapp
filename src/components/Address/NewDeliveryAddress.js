import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView,
    Dimensions,
} from 'react-native';
import Input from '../SignIn/Input';
import { TouchableHighlight } from 'react-native-gesture-handler';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class NewDeliveryAddress extends Component {

    onCLickHandler = () => {
        this.props.navigation.navigate('SignIn2')
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior='padding' enabled>
                <ScrollView>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>New Delivery Address</Text>
                    </View>
                    <View style={{ paddingTop: 36 }} >
                        <Input placeholder='Full Name' keyboardType='default' />
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='Phone Number' keyboardType='phone-pad' />
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='Pin Code' keyboardType='number-pad' />
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='Flat / House / Building Name' keyboardType='default' />
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='Colony / Street / Locality' keyboardType='default' />
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='Landmark (Optional)' keyboardType='default' />
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='City' keyboardType='default' />
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Input placeholder='State' keyboardType='default' />
                        </View>
                        <TouchableHighlight underlayColor='#fff' onPress={this.onCLickHandler} >
                            <View style={{ paddingTop: 50 }}>
                                <View style={styles.buttonLayout}>
                                    <Text style={styles.buttonText}>Proceed to Payment</Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 20,
    },
    heading: {
        color: '#581400',
        fontWeight: '600',
        fontSize: 18
    },
    headingContainer: {
        paddingTop: 20,
        flex: 1,
        alignItems: 'center',
    },
    buttonLayout: {
        backgroundColor: '#C5C5C5',
        width: width * 85 / 100,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    buttonText: {
        color: '#919191',
        fontSize: 14

    },
})


export default NewDeliveryAddress;