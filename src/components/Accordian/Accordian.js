import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const data = [
    {
        'que': 'Easy and Free Return',
        'ans': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor.'
    },
    {
        'que': 'Payment Every Friday',
        'ans': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor.'
    },
    {
        'que': 'Free Shipping and COD',
        'ans': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor.'
    },
    {
        'que': 'Quality Assured Products',
        'ans': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor.'
    },
    {
        'que': 'Delivery in 10 Days',
        'ans': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor.'
    },

]
export default class Accordian extends Component {

    state = {
        show: []
    }

    componentDidMount = () => {
        var show = this.state.show;
        for (let i = 0; i < data.length; i++) {
            show.push('false')
        }
        this.setState({ show })
    }

    onClickHandler = (index) => {
        var show = this.state.show;
        console.log(show)

        for (let i = 0; i < data.length; i++) {
            if (show[i] === 'true') {
                show[i] = false
            }
        }
        for (let i = 0; i < data.length; i++) {
            if (i === index) {
                if (show[index] === 'false') {
                    show[index] = 'true'
                } else {
                    show[index] = 'false'
                }
            }
        }
        this.setState({ show })

    }


    renderData = () => {
        return data.map((data, i) => {
            return (
                <View style={styles.innerContainer} key={i}>
                    <TouchableHighlight underlayColor="#fff" onPress={() => this.onClickHandler(i)} >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 25 }}>
                            <Text style={{ color: '#581400', fontSize: 11 }}>{data.que}</Text>
                            {this.state.show[i] == 'true' ? <Text style={{ color: '#581400', fontSize: 11 }}>hide</Text> : <Text style={{ color: '#581400', fontSize: 11 }}>show</Text>}
                        </View>
                    </TouchableHighlight>
                    {this.state.show[i] == 'true' ?
                        <TouchableHighlight underlayColor="#fff" onPress={() => this.onClickHandler(i)} >
                            <View>
                                <Text style={{ paddingTop: 10, paddingBottom: 10, color: '#A77248', fontSize: 11 }}>{data.ans}</Text>
                            </View>
                        </TouchableHighlight> : console.log('hey')}
                    <View style={styles.line}></View>
                </View>
            )
        })
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderData()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    innerContainer: {
        width: width * 90 / 100,
        height: 'auto'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 10,
        marginBottom: 80

    },
    line: {
        width: width * 90 / 100,
        borderWidth: 1,
        borderColor: '#CB9F68',
        marginVertical: 6
    }
})
