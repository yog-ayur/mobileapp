import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, ScrollView, KeyboardAvoidingView, } from 'react-native';
import TopHeader from '../Header/TopHeader';
import { withNavigation } from 'react-navigation';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;


const product = {
    name: 'Product_Name',
    description: 'Lorem ipsum dolor sit amet, dconsetetur sadipscing elitr, sed diam nonumy eirmod et tempor invidunt ut labore et dolore magna menil aliquyam erat, sed diam voluptua. At vero eos et loret justo duo dolores et ea rebum. Stet clita kasd gubergren, nothre sea takimata sanctus est Lorem ipsum dolor sit amet. Alguti Lorem ipsum dolor sit amet, consetetur sadipscing.',
    rating: 2.5,
    delivery: '5-7 days',
    dispatch: '2-3 days'
}


 class Product extends Component {

    renderProduct = () => {
        return (
            <ScrollView>
                <View style={styles.headingContainer}>
                    <View style={styles.innerBox}>
                        <Image source={require('../../../assets/product.png')} style={styles.productImage} />
                    </View>
                </View>

                <View style={styles.descriptionBox}>
                    <View style={{ width: width * 45 / 100, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ color: '#581400', fontSize: 16, fontWeight: 'bold' }}>Catalog Description</Text>
                        </View>
                    </View>

                    <View style={styles.ratingBox}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={{ color: '#581400', fontWeight: 'bold', fontSize: 13 }}>{product.rating}</Text>
                            <Image source={require('../../../assets/fillStar.png')} style={styles.starImage} resizeMode='contain' />
                            <Image source={product.rating > 1.0 ? require('../../../assets/fillStar.png') : require('../../../assets/emptyStar.png')} style={styles.starImage} resizeMode='contain' />
                            <Image source={product.rating > 2.0 ? require('../../../assets/fillStar.png') : require('../../../assets/emptyStar.png')} style={styles.starImage} resizeMode='contain' />
                            <Image source={product.rating > 3.0 ? require('../../../assets/fillStar.png') : require('../../../assets/emptyStar.png')} style={styles.starImage} resizeMode='contain' />
                            <Image source={product.rating > 4.0 ? require('../../../assets/fillStar.png') : require('../../../assets/emptyStar.png')} style={styles.starImage} resizeMode='contain' />
                        </View>
                    </View>
                </View >

                <View style={styles.descriptionText}>
                    <Text style={{ color: '#581400', fontSize: 14 }}>{product.description}</Text>
                </View>

                <View style={{ borderColor: '#FFE598', width: width * 90 / 100, borderWidth: 1, marginVertical: 12 }}></View>

                <View style={styles.deliveryBox}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.deliveryText}>Delivery: </Text>
                        <Text style={styles.deliveryText}>{product.delivery}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.deliveryText}>Dispatch: </Text>
                        <Text style={styles.deliveryText}>{product.dispatch}</Text>
                    </View>
                    <View style={styles.copyBox}>
                        <View style={styles.copy}>
                            {/* <Image source={require('')} /> */}
                            <Text style={styles.deliveryText}>Copy Description</Text>
                        </View>
                        <View style={styles.copy}>
                            {/* <Image source={require('')} /> */}
                            <Text style={styles.deliveryText}>Add to website</Text>
                        </View>
                    </View>
                </View>

                <View style={{ width: width * 90 / 100, flexDirection: 'row', paddingTop: 14 }}>

                    <View style={{ width: width * 45 / 100, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image source={require('../../../assets/download.png')} style={styles.likeImage} />
                        <Image source={require('../../../assets/like.png')} style={[styles.likeImage, { marginLeft: 10 }]} />
                        <Image source={require('../../../assets/facebook.png')} style={[styles.likeImage, { marginLeft: 10 }]} />
                        <Image source={require('../../../assets/facebook.png')} style={[styles.likeImage, { marginLeft: 10 }]} />
                    </View>

                    <View style={{ width: width * 45 / 100, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={styles.whatsapp}>
                            <Image source={require('../../../assets/whatsapp.png')} style={{ width: 20, height: 20 }} resizeMode='contain' />
                            <Text style={styles.whatsappText}>Share & Earn</Text>
                        </View>
                    </View>

                </View>
                <View style={{ borderColor: '#FFE598', width: width * 90 / 100, borderWidth: 1, marginVertical: 8 }}></View>

                <View style={styles.copyBox}>
                    <View style={[styles.cartBox, { backgroundColor: '#FFE598' }]}>
                        <Text style={[styles.cartText, { color: '#A77248' }]}>Add to Cart</Text>
                    </View>
                    <View style={[styles.cartBox, { backgroundColor: '#A77248' }]}>
                        <Text style={[styles.cartText, { color: '#FFE598' }]}>Buy Now</Text>
                    </View>
                </View>
                <View style={{ borderColor: '#FFE598', width: width * 90 / 100, borderWidth: 1, marginVertical: 8 }}></View>

            </ScrollView>
        )
    }

    render() {
        return (
            <View style={styles.container} >
                <TopHeader text={product.name} />
                <ScrollView >
                    <View style={styles.innerContainer}>

                        {this.renderProduct()}
                    </View>
                </ScrollView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',

    },
    innerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    descriptionBox: {
        width: width * 90 / 100,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10
    },
    ratingBox: {
        width: width * 45 / 100,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: 13
    },
    headingContainer: {
        flex: 1,
        alignItems: 'center',
    },
    productImage: {
        resizeMode: 'contain',
        width: width * 40 / 100,
        height: height * 40 / 100,
    },
    descriptionText: {
        width: width * 90 / 100,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 10
    },
    topRow: {
        width: width * 90 / 100,
        height: height * 30 / 100,
        margin: 4,
        marginTop: 25,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    innerBox: {
        width: width * 90 / 100,
        height: height * 30 / 100,
        borderLeftWidth: 16,
        borderRightWidth: 15,
        borderColor: '#D8D8DB',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    starImage: {
        width: 10,
        height: 10,
        margin: 2,
        resizeMode: 'contain'
    },
    likeImage: {
        width: '20%',
        height: 24,
        resizeMode: 'contain'
    },
    whatsapp: {
        width: 130,
        height: 36,
        backgroundColor: '#A3BA3D',
        borderRadius: 18,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    whatsappText: {
        color: '#fff',
        fontSize: 12,
        fontWeight: '400',
        marginLeft: 7
    },
    deliveryBox: {
        width: width * 90 / 100,
        height: 'auto',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    deliveryText: {
        color: '#581400',
        fontWeight: 'normal',
        fontSize: 10
    },
    copy: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#CB9F68',
        width: width * 43 / 100,
        height: height * 6 / 100,
        borderRadius: 4
    },
    copyBox: {
        flexDirection: 'row',
        width: width * 90 / 100,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 14
    },
    cartBox: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: width * 43 / 100,
        height: height * 6 / 100,
        borderRadius: 20
    },
    cartText: {
        fontSize: 14,
        fontWeight: 'bold',
    }


});


export default Product;