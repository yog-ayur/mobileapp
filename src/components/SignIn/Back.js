import React, { Component } from "react";
import { View, StyleSheet, TouchableWithoutFeedback } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Back extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={() => alert("back button")}>
          <Icon name="md-arrow-back" style={styles.btn}/>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
    display: "flex",
    alignItems:"flex-start",
    fontWeight: "normal",
  },
  btn:{
    margin: 20,
    fontSize: 25,
    color:'#581400'
  }
});
