import React, { Component } from 'react';
import DrawerNavigator from '../../navigation/DrawerNavigator';

class Home extends Component {

  render() {
    return (
      <DrawerNavigator />
    );
  }
}

export default Home;
