import React, { Component } from 'react';
import { View, Dimensions, StyleSheet, Image, ScrollView, Button, Text } from 'react-native';

let height = Dimensions.get('window').height;
let width = Dimensions.get('window').width;

class Congrats extends Component {
    render() {
        return (
            <>
                <View style={styles.UpperCover}>
                    <View style={styles.Circle3} />
                    <View style={styles.Circle2} />
                    <View style={styles.InnerCircle} />
                    <Image style={styles.CheckImage} source={{ uri: 'https://facebook.github.io/react-native/img/tiny_logo.png'}} />
                    
                    <View>
                        <Text style={styles.TextHeading}>Congratulations</Text>
                        <Text style={styles.RestText}>your order has been placed successfully.</Text>
                    </View>
                </View>

                <View style={styles.LowerCover}>
                    <View>
                        <View style={styles.ViewDetailsCover}>
                            <Text style={styles.ViewDetailsButton}>View Details</Text>
                        </View>
                        <View style={styles.KeepShoppingCover}>
                            <Text style={styles.KeepShoppingButton}>View Details</Text>
                        </View>
                    </View>
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    UpperCover: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    InnerCircle: {
        position: 'absolute',
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor: '#FDF298'
    },
    Circle2: {
        position: "absolute",
        width: 135,
        height: 135,
        borderRadius: 135 / 2,
        backgroundColor: '#FDF7BB'
    },
    Circle3: {
        position: "absolute",
        width: 170,
        height: 170,
        borderRadius: 170 / 2,
        backgroundColor: '#FEFCDF'
    },
    CheckImage: {
        position: 'absolute',
        height: 40,
        width: 60,
    },
    TextHeading: {
        fontWeight: 'bold',
        fontSize: 22,
        color: '#681713',
        textAlign: 'center',
        marginTop: 250
    },
    RestText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#681713',
    },
    LowerCover: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    ViewDetailsCover: {
        width: width * 85 / 100,
        height: height * 6 / 100,
        backgroundColor: '#AB7249',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        marginBottom: 10
    },
    KeepShoppingCover: {
        width: width * 85 / 100,
        height: height * 6 / 100,
        backgroundColor: '#F1E18D',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
    },
    ViewDetailsButton: {
        color: '#F1E18D',
        fontWeight: 'bold',
        fontSize: 14,
    },
    KeepShoppingButton: {
        color: '#AB7249',
        fontWeight: 'bold',
        fontSize: 14,
    }
});

export default Congrats;