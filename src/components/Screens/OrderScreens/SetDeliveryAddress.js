import React, { Component } from 'react';
import { View, Dimensions, StyleSheet, Image, ScrollView, Button, Text } from 'react-native';
import { savedAddress } from './addressdata.json';
import { FlatList } from 'react-native-gesture-handler';

let height = Dimensions.get('window').height;
let width = Dimensions.get('window').width;

class PrevAddress extends Component {
  

    render() {
        return (
            <View>
                <FlatList data={savedAddress}
                    renderItem={({ item }) => {
                        // console.log("Here are the details : " + item.name);
                        return (
                            <View style={styles.Address}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent : 'space-between',
                                    alignContent : 'center',
                                }}>
                                <Text style={styles.OrderBy}>{item.name}{"\n"}</Text>
                                <Text style={styles.selectAddress}>Select</Text>
                                </View>
                                <Text style={styles.OrderAddress}>
                                    {item.address}{"\n"}
                                    {item.city}, {item.pincode}{"\n"}
                                    {item.phone}{"\n"}{"\n"}
                                </Text>
                                <View style={styles.EditDeleteAddress}>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row'
                                    }}>
                                        <Image style={styles.EditImage} source={require('./../../../../assets/Search.png')} />
                                        <Text style={styles.EditAddress}>Edit</Text>
                                    </View>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row'
                                    }}>
                                        <Image style={styles.DeleteImage} source={require('./../../../../assets/Search.png')} />
                                        <Text style={styles.DeleteAddress}>Delete</Text>
                                    </View>
                                </View>
                            </View>
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
        // }

    }
}


class SetDeliveryAddress extends Component {
    render() {
        return (
            <>
                <View styles={styles.UpperCover}>

                    <View style={styles.HeadingLine}>
                        <Text style={styles.ArrowButton}>&#x2B05;</Text>
                        <Text style={styles.PageHeading}>Set Delivery Address</Text>
                    </View>

                    <PrevAddress />
                </View>

                <View style={styles.LowerCover}>
                    <View style={styles.ViewDetailsCover}>
                        <Text style={styles.ViewDetailsButton}>+   New Delivery Address</Text>
                    </View>
                </View>

            </>
        );
    }
}

const styles = StyleSheet.create({
    UpperCover: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    HeadingLine: {
        flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-around',
        paddingVertical: 50,
    },
    ArrowButton: {
        color: '#AB7249',
        fontSize: 21
    },
    PageHeading: {
        color: '#AB7249',
        fontSize: 22,
        fontWeight: 'bold',
        marginRight: 50
    },
    Address: {
        backgroundColor: '#FEFEEF',
        paddingHorizontal: 20,
        borderColor: '#EBEBE0',
        borderWidth: 0.5,
        borderRadius: 15,
        paddingVertical: 20,
        marginBottom: 10
    },
    OrderBy: {
        color: '#AB7249',
        fontSize: 18,
        fontWeight: 'bold',
        paddingHorizontal: 20
    },
    selectAddress: { 
        color: '#AB7249',
        fontSize: 18,
        fontWeight: 'bold',

    },
    OrderAddress: {
        color: '#BE936F',
        fontSize: 17,
        paddingHorizontal: 20
    },
    EditDeleteAddress: {
        flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 20
    },
    EditImage: {
        height: 22,
        width: 22
    },
    EditAddress: {
        fontSize: 15,
        color: '#AB7249',
        fontWeight : '500'
    },
    DeleteImage: {
        height: 22,
        width: 22
    },
    DeleteAddress: {
        fontSize: 15,
        color: '#AB7249',
        fontWeight: '500'
    },
    LowerCover: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    ViewDetailsCover: {
        width: width * 85 / 100,
        height: height * 6 / 100,
        backgroundColor: '#AB7249',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
    },
    ViewDetailsButton: {
        color: '#F1E18D',
        fontWeight: 'bold',
        fontSize: 14,
    },
});

export default SetDeliveryAddress;