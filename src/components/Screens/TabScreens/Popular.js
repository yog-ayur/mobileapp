import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, ScrollView } from 'react-native';
import TopHeader from '../../Header/TopHeader';
import Carousal from '../../Carousal/Carousal';
import CommonBanner from '../../Banner/CommonBanner';
import { TouchableHighlight } from 'react-native-gesture-handler';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const GET_CATEGORY = gql`
query {
  categories(first: 3){
    edges {
      node {
        name
        id
      }
    }  
  }
}`;

var images = [

  require('../../../../assets/slider3.png'),
  require('../../../../assets/slider2.png'),
  require('../../../../assets/slider3.png'),
  require('../../../../assets/slider2.png'),
  require('../../../../assets/slider3.png'),
  require('../../../../assets/slider2.png'),
  require('../../../../assets/slider3.png'),
  require('../../../../assets/slider2.png'),
  require('../../../../assets/slider3.png'),
]


const images2 = [
  {
    image: require('../../../../assets/image.png')
  },
  {
    image: require('../../../../assets/image.png')
  },
  {
    image: require('../../../../assets/image.png')
  },
  {
    image: require('../../../../assets/image.png')
  },

];
const categories = [
  {
    category: 1
  },
  {
    category: 2
  },
  {
    category: 3
  }
]

const cards = [
  {
    card: 1
  },
  {
    card: 2
  },
  {
    card: 3
  }
]

class Popular extends Component {

  showCategories = () => {
    return categories.map((category, i) => {
      return (
        <View key={i} style={styles.greyBox}></View>
      )
    })
  }

  showUICategories = () => {
    return this.props.data.categories.edges.map((category, i) => {
      return (
        <View key={i} style={styles.greyBox}></View>
      )
    })
  }

  showBox = () => {
    return (
      <View style={{ flexDirection: 'row', width: width * 100 / 100, paddingLeft: 8 }}>
        {this.showCategories()}
      </View>
    )
  }

  showCategoryBox = () => {
    return (
      <View style={{ flexDirection: 'row', width: width * 100 / 100, paddingLeft: 8 }}>
        {this.showUICategories()}
      </View>
    )
  }

  onPressHandler=()=>{
    console.log(  this.props.navigation.navigate(''))
    this.props.navigation.navigate('Product')
  }

  showCards = () => {
    return cards.map((card, i) => {
      return (

        <View key={i} style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 15 }}>
          <View style={styles.card}>
            <TouchableHighlight underlayColor='#fff' onPress={this.onPressHandler} >
              <Text style={styles.itemsText}>Live Healthy Ayurvedic Green Tea (250gm)</Text>
            </TouchableHighlight>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <View style={styles.innerBox}>
                <Image source={require('../../../../assets/product.png')} style={{ height: 134, width: 134 }} resizeMode='contain' />
              </View>
              <View style={styles.innerBox2}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={styles.smallBox1}></View>
                  <View style={styles.smallBox2}></View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={styles.smallBox3}></View>
                  <View style={styles.smallBox4}>
                    <Text style={styles.two}>+2</Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={{ width: width * 82 / 100, flexDirection: 'row' }}>

              <View style={{ width: width * 41 / 100, flexDirection: 'row', justifyContent: 'flex-start' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ color: '#581400', fontSize: 14, fontWeight: 'bold' }}>₹450</Text>
                  <Text style={styles.deletedText}>₹650</Text>
                  <View style={{ width: 25, height: 1, backgroundColor: '#A77248', position: 'absolute', marginLeft: 40, marginTop: 10 }}></View>
                </View>
              </View>

              <View style={{ width: width * 41 / 100, flexDirection: 'row', justifyContent: 'flex-end', paggingTop: 13 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                  <Text style={{ color: '#581400', fontWeight: 'bold', fontSize: 13 }}>4.0</Text>
                  <Image source={require('../../../../assets/fillStar.png')} style={styles.starImage} resizeMode='contain' />
                  <Image source={require('../../../../assets/fillStar.png')} style={styles.starImage} resizeMode='contain' />
                  <Image source={require('../../../../assets/fillStar.png')} style={styles.starImage} resizeMode='contain' />
                  <Image source={require('../../../../assets/fillStar.png')} style={styles.starImage} resizeMode='contain' />
                  <Image source={require('../../../../assets/emptyStar.png')} style={styles.starImage} resizeMode='contain' />
                </View>
              </View>

            </View >

            <View style={{ width: width * 82 / 100, flexDirection: 'row', paddingTop: 14 }}>

              <View style={{ width: width * 41 / 100, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Image source={require('../../../../assets/download.png')} style={styles.likeImage} />
                <Image source={require('../../../../assets/like.png')} style={[styles.likeImage, { marginLeft: 10 }]} />
                <Image source={require('../../../../assets/facebook.png')} style={[styles.likeImage, { marginLeft: 10 }]} />
              </View>

              <View style={{ width: width * 41 / 100, flexDirection: 'row', justifyContent: 'flex-end' }}>
                <View style={styles.whatsapp}>
                  <Image source={require('../../../../assets/whatsapp.png')} style={{ width: 20, height: 20 }} resizeMode='contain' />
                  <Text style={styles.whatsappText}>Share & Earn</Text>
                </View>
              </View>

            </View>

          </View>
        </View>
      )
    })
  }

  render() {
    const { data } = this.props;
    return (
      <View style={{ flex: 1, justifyContent: 'center', }} >
        <TopHeader text='Logo' search='null' />
        <ScrollView behaviour='padding' showsVerticalScrollIndicator={false}>
          <Carousal images={images} text='carousal1' />
          <CommonBanner images={images2} />
          {this.showBox()}
          <View>
            <Text style={styles.categoryText}>Show All Categories</Text>
          </View>
          <View>
            <Text style={styles.recommendText}>Recommended for you</Text>
          </View>
          {this.showBox()}
          <View>
            <Text style={styles.categoryText}>View More</Text>
          </View>
          <View>
            <Text style={styles.recommendText}>Popular Products</Text>
          </View>
          {this.showBox()}
          {this.showBox()}
          <View>
            <Text style={styles.categoryText}>View More</Text>
          </View>
          {this.showCards()}
          <View style={{ justifyContent: 'center', alignItems: 'center', padding: 20 }}>
            <View style={styles.banner}>
              <Text style={styles.bannerText}>Promotional / Advertisement banners</Text>
            </View>
          </View>
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  greyBox: {
    width: width * 26.2 / 100,
    height: height * 20 / 100,
    backgroundColor: '#C5C5C5',
    margin: 10,
    borderRadius: 4
  },
  categoryText: {
    fontSize: 11,
    color: '#A77248',
    textAlign: 'right',
    paddingHorizontal: 20
  },
  recommendText: {
    color: '#581400',
    fontSize: 13,
    textAlign: 'center'
  },
  card: {
    width: width * 90 / 100,
    height: height * 45 / 100,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#D8D8DB',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    display: 'flex'
  },
  itemsText: {
    fontSize: 14,
    fontWeight: '400',
    color: '#581400',
    fontSize: 14,
    textAlign: 'center',
    padding: 10
  },
  innerBox: {
    width: width * 41 / 100,
    height: height * 24 / 100,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#D8D8DB',
    margin: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  innerBox2: {
    width: width * 41 / 100,
    height: height * 24 / 100,
    margin: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  smallBox1: {
    width: width * 20.4 / 100,
    height: height * 11.8 / 100,
    backgroundColor: '#D8D8DB',
    borderTopLeftRadius: 4,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderColor: '#fff'
  },
  smallBox2: {
    width: width * 20.4 / 100,
    height: height * 11.8 / 100,
    borderTopRightRadius: 4,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    backgroundColor: '#D8D8DB',
    borderColor: '#fff'

  },
  smallBox3: {
    width: width * 20.4 / 100,
    height: height * 11.8 / 100,
    backgroundColor: '#D8D8DB',
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderColor: '#fff',
    borderBottomLeftRadius: 4,

  },
  smallBox4: {
    width: width * 20.4 / 100,
    height: height * 11.8 / 100,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    backgroundColor: '#8D8D8D',
    borderColor: '#fff',
    borderBottomRightRadius: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  two: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 18
  },
  deletedText: {
    fontSize: 10,
    color: '#A77248',
    marginHorizontal: 10
  },
  starImage: {
    width: 10,
    height: 10,
    margin: 2,
    resizeMode: 'contain'
  },
  likeImage: {
    width: '20%',
    height: 24,
    resizeMode: 'contain'
  },
  whatsapp: {
    width: 130,
    height: 36,
    backgroundColor: '#A3BA3D',
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  whatsappText: {
    color: '#fff',
    fontSize: 12,
    fontWeight: '400',
    marginLeft: 7
  },
  banner: {
    width: width * 90 / 100,
    borderRadius: 4,
    height: height * 20 / 100,
    backgroundColor: '#C5C5C5',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bannerText: {
    color: '#fff',
    fontSize: 12
  }
})

const PopularWrapper = graphql(GET_CATEGORY)(Popular);

export default PopularWrapper;