import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet ,ScrollView} from 'react-native';
import TopHeader from '../../Header/TopHeader';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;


class Collections extends Component {

  render() {
    return (
      <View style={{ flex: 1,paddingBottom:20}}>
        <TopHeader text='Collections' />
        <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.greyBox}></View>
            <View style={styles.greyBox}></View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.greyBox}></View>
            <View style={styles.greyBox}></View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.greyBox}></View>
            <View style={styles.greyBox}></View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: width * 100 / 100,
    flexDirection: 'column'
  },
  greyBox: {
   width:width*43/100,
   height:height*25/100,
   backgroundColor:'#D8D8DB',
   marginLeft:16,
   borderRadius:4,
   marginVertical:10
  }
});


export default Collections;