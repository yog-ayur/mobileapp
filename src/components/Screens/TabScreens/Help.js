import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image } from 'react-native';
import TopHeader from '../../Header/TopHeader';
import { TouchableHighlight, ScrollView } from 'react-native-gesture-handler';
import Accordian from '../../Accordian/Accordian';



const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const data = ['Learning Center', 'Contact Us']

const videos=['1','2','3','4']

export default class Help extends Component {

  state = {
    active: [],
  }

  componentDidMount = () => {
    var active = this.state.active
    active = ['true', 'false']
    this.setState({ active })
  }

  onPressHandler = (index) => {
    var active = this.state.active

    for (let k = 0; k <= data.length; k++) {
      if (active[k] === 'true') {
        active[k] = 'false'
      }
    }

    for (let i = 0; i <= data.length; i++) {
      if (i === index) {
        if (active[i] === 'false') {
          active[i] = 'true'

        } else {
          active[i] = 'false'
        }
      }
    }
    this.setState({ active })
    console.log(this.state.active)
  }

  renderData = () => {
    console.log(this.state.active)
    return data.map((data, i) => {
      return (
        <View key={i}>
          <TouchableHighlight underlayColor='#fff' onPress={() => this.onPressHandler(i)} >
            <View style={this.state.active[i] === 'true' ? styles.activeBox : styles.inactiveBox}>
              <Text style={this.state.active[i] === 'true' ? styles.activeText : styles.inactiveText}>{data}</Text>
            </View>
          </TouchableHighlight>

        </View>
      )
    })
  }

  renderText = () => {
    return data.map((data, i) => {
      return (
        <View key={i}>
          {this.state.active[i] === 'true' && i === 0 ?
            <View>
              <View style={styles.welcomeBox}>
                <Text style={styles.welcomeText1}>Welcome to Yog Ayur Learning Centre</Text>
                <Text style={styles.welcomeText2}>Watch these videos and learn everything about reselling</Text>
              </View>
              <View style={{justifyContent:'center',alignItems:'center'}}>
                {this.watchVideo()}
              </View>
            </View>
            : this.state.active[i] === 'true' && i === 1 ?
            <View style={{alignItems:'center'}}>

              <View style={[styles.welcomeBox, { height: height * 25 / 100 }]}>
                <View style={styles.callBox}>
                  <Image source={require('../../../../assets/call.png')} style={{ width: 20, height: 20, resizeMode: 'contain', marginHorizontal: 12 }} />
                  <Text style={{ fontSize: 12, color: '#fff' }}>Call Support (012-12345678)</Text>
                </View>
                <View style={styles.callBox}>
                  <Image source={require('../../../../assets/sms.png')} style={{ width: 20, height: 20, resizeMode: 'contain', marginHorizontal: 12 }} />
                  <Text style={{ fontSize: 12, color: '#fff' }}>Chat with Support</Text>
                </View>
                <Text style={{ color: '#A77248', fontSize: 10 }}>support available between 10AM to 8PM</Text>
              </View>
              <View style={{width:width*90/100,height:height*3/100,marginVertical:10}}>
                <Text style={{color:'#581400',fontSize:12,fontWeight:'bold'}}>Yog Ayur Benifits</Text>
              </View>
              <Accordian />
              </View>
              : console.log('hey')}
        </View>
      )
    }
    )
  }

  watchVideo = () => {
    return videos.map((video,i)=>{
    return (
      <View key={i} style={{ flexDirection: 'column',justifyContent:'center',width:width*90/100, paddingTop:10}}>
        <Text style={ [styles.videoText,{fontWeight:'bold'}]}>Watch & learn about Yog Ayur reselling</Text>
        <Text style={styles.videoText}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</Text>
        <View style={styles.videoBox}></View>
        <Text style={styles.videoText}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.</Text>
        <View style={{borderColor:'#FFE598',width:width*90/100,borderWidth:1,marginVertical:12}}></View>
      </View>
    )
  })
}

  render() {
    return (
      <View style={{ flex: 1 }}>
        <TopHeader text='Help Centre' />
        <ScrollView>
        <View style={styles.container}>
          {this.renderData()}
        </View>
        <View>
          {this.renderText()}
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width,
    paddingTop: 10
  },
  inactiveBox: {
    width: width * 50 / 100,
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inactiveText: {
    fontSize: 12,
    color: '#8D8D8D'
  },
  activeText: {
    color: '#CB9F68',
    fontSize: 12,
    fontWeight: '600'
  },
  activeBox: {
    width: width * 50 / 100,
    alignItems: 'center',
    borderBottomWidth: 2,
    height: 30,
    borderColor: '#CB9F68',
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcomeBox: {
    backgroundColor: '#FFF2CB',
    width: width,
    height: height * 10 / 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  welcomeText1: {
    color: '#A77248',
    fontSize: 12,
    fontWeight: '500'
  },
  welcomeText2: {
    color: '#A77248',
    fontSize: 8,
    paddingVertical: 4
  },
  callBox: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 75 / 100,
    height: '27%',
    backgroundColor: '#CB9F68',
    borderRadius: 20,
    flexDirection: 'row',
    marginVertical: 8
  },
  videoBox: {
    width: width * 90 / 100,
    height: height*25/100,
    backgroundColor: '#F1F1F1',
    borderRadius: 4,
    marginVertical:8

  },
  videoText:{
    fontSize:12,
    color:'#581400',
  }


})
