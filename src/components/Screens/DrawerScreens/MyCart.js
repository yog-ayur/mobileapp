import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView } from 'react-native';
import TopHeader from '../../Header/TopHeader';
import { TouchableHighlight } from 'react-native-gesture-handler';
import SetDeliveryAddress from '../OrderScreens/SetDeliveryAddress';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const product = [
  {
    name: 'Product Name',
    quantity: '1',
    price: '$450',
    ship: '$29',
    editMargin: '$1',
    total: '$480',
    image: require('../../../../assets/product.png')
  },
  {
    name: 'Product Name',
    quantity: '1',
    price: '$450',
    ship: '$29',
    editMargin: '$1',
    total: '$480',
    image: require('../../../../assets/product.png')

  },

]



class MyCart extends Component {

  renderData = () => {
    return product.map((product, i) => {
      return (

        <View style={styles.card} key={i}>
          <View style={styles.innerCard}>

            <View style={{ flexDirection: 'column', width: width * 45 / 100 }}>
              <View style={styles.pNameBox}>
                <Text style={styles.productName}>{product.name}</Text>
              </View>
              <View style={styles.textBox}>
                <Text style={styles.productText}>Quantity:</Text>
              </View>
              <View style={styles.textBox}>
                <Text style={styles.productText}>Supply Price:</Text>
                <Text style={styles.productText} >{product.price}</Text>
              </View>
              <View style={styles.textBox}>
                <Text style={styles.productText}>Shipping</Text>
                <Text style={styles.productText}>{product.ship}</Text></View>
              <View style={styles.textBox}>
                <Text style={styles.productText}>Edit Margin:</Text>
                <Text style={styles.productText}>{product.editMargin}</Text>
              </View>
            </View>

            <View style={{ width: width * 35 / 100 }}>
              <View style={styles.imageBox}>
                <Image source={product.image} resizeMode='contain' style={styles.productImage} />
              </View>
            </View>

          </View>

          <View style={{ width: width * 85 / 100, }}>
            <View style={{ width: width * 45 / 100, flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={[styles.productText, { fontWeight: '500' }]}>Sub Total</Text>
              <Text style={[styles.productText, { fontWeight: '500' }]}>{product.total}</Text>
            </View>
          </View>
        </View>
      )
    })
  }


  render() {
    return (
      <View style={{ flex: 1 }}>
        <TopHeader text='Cart' />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {this.renderData()}
            <View style={styles.coupenBox}>
              <View style={styles.innerBox}>
                <View style={styles.coupenType}></View>
                <View style={styles.applyBox}><Text style={styles.coupenText}>Apply Coupon</Text></View>
              </View>
            </View>
            <View style={styles.totalBox}>
              <View style={styles.totalTextBox}>
                <Text style={styles.totalMargin}>Total Margin:</Text>
                <Text style={styles.totalMargin}>$2</Text>
              </View>
              <View style={styles.totalTextBox}>
                <Text style={styles.totalPrice}>Total Price:</Text>
                <Text style={styles.totalPrice}>$960</Text>
              </View>
            </View>
           <TouchableHighlight underlayColor="#fff" onPress={()=>this.props.navigation.navigate('SetDeliveryAddress')} >
           <View style={styles.address}>
              <Text style={styles.addressText}>Set Delivery Address</Text>
            </View>
           </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom:20
  },
  card: {
    width: width * 90 / 100,
    height: height * 30 / 100,
    borderWidth: 1,
    borderColor: '#0000001A',
    borderRadius: 4,
    alignItems: 'center',
    marginBottom: 12,

  },
  innerCard: {
    width: width * 85 / 100,
    height: height * 20.5 / 100,
    borderBottomWidth: 1,
    borderColor: '#FFF2CB',
    flexDirection: 'row',
    marginVertical: 6,
    justifyContent: 'space-between'
  },
  imageBox: {
    borderColor: '#D8D8DB',
    borderBottomWidth: 2.28,
    borderRightWidth: 5,
    borderTopWidth: 2,
    borderLeftWidth: 5.26,
    borderRadius: 4,
    width: width * 34 / 100,
    height: height * 19 / 100,

  },
  productImage: {
    width: width * 33 / 100,
    height: height * 18 / 100,
  },
  productText: {
    color: '#581400',
    fontSize: 12,
  },
  productName: {
    fontWeight: '500',
    color: '#581400',
    fontSize: 14
  },
  textBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 4
  },
  pNameBox: {
    paddingBottom: 6
  },
  coupenBox: {
    width: width * 90 / 100,
    height: height * 10 / 100,
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderColor: '#FFF2CB',
    alignItems: 'center',
    justifyContent: 'center'
  },
  innerBox: {
    width: width * 90 / 100,
    height: height * 6 / 100,
    borderWidth: 1,
    borderColor: '#D8D8DB',
    borderRadius: 18,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'center'
  },
  applyBox: {
    borderRadius: 18,
    width: width * 35 / 100,
    height: height * 6 / 100,
    backgroundColor: '#FFE598',
    justifyContent: 'center',
    alignItems: 'center',
  },
  coupenType: {
    width: width * 55 / 100,
    height: height * 6 / 100,
  },
  coupenText: {
    color: '#A77248',
    fontSize: 12,
    fontWeight: 'bold'
  },
  totalBox: {
    justifyContent: 'space-between',
    width: width * 90 / 100,
    height: 'auto',
    flexDirection: 'column',
    marginVertical: 5

  },
  totalTextBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5
  },
  totalMargin: {
    color: '#581400',
    fontSize: 12,
  },
  totalPrice: {
    color: '#581400',
    fontWeight: 'bold',
    fontSize: 14
  },
  address: {
    width: width * 90 / 100,
    height: height * 6/ 100,
    backgroundColor: '#A77248',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 18,
    marginTop:50,
    
  },
  addressText:{
    color:'#FFE598',
    fontWeight:'bold',
    fontSize:14,
  }

})


export default MyCart;