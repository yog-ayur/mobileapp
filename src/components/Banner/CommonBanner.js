import React, { Component } from 'react';
import Carousel from 'react-native-banner-carousel';
import { StyleSheet, Image, View, Text, Dimensions, TouchableHighlight } from 'react-native';
import { withNavigation } from 'react-navigation'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;



class CommonBanner extends Component {
    renderPage(image, text, heading, index) {
        return (
            <View key={index} style={styles.innerContainer}>
                {this.props.heading && <View style={styles.headerContainer}>
                    <Text style={styles.headerText}>{heading}</Text>
                    <View style={{ width: width * 80 / 100, marginTop: 10 }}>
                        <Text style={{ textAlign: 'justify', color: '#A77248' }}>{text}</Text>
                    </View>
                </View>}
                <Image style={this.props.heading ? styles.image : styles.image2} source={image} resizeMode='contain' />
                {this.props.heading && <TouchableHighlight underlayColor='#fff' onPress={() => this.props.navigation.navigate('SignIn2')}>
                    <View style={styles.startContainer}>
                        <Text style={styles.startText}>Get Started</Text>
                    </View>
                </TouchableHighlight>}
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={width}
                    showsPageIndicator={this.props.heading ? true : false}
                    pageIndicatorStyle={{ height: 10, width: 10, borderRadius: 10, backgroundColor: '#FFE598' }}
                    pageIndicatorOffset={20}
                    activePageIndicatorStyle={{ backgroundColor: '#A77248' }}
                >
                    {this.props.images.map((image, index) => this.renderPage(image.image, image.text, image.heading, index))}
                </Carousel>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    innerContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    startContainer: {
        width: width * 80 / 100,
        height: (height * 6) / 100,
        backgroundColor: '#A77248',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        marginTop: 10
    },
    startText: {
        color: 'white',
        fontWeight: '400'
    },
    image: {
        width: (width * 90) / 100,
        height: (height * 65) / 100
    },
    image2: {
        width: (width * 90) / 100,
        height: (height * 35) / 100,
        borderRadius: 7

    },
    headerContainer: {
        height: height * 20 / 100,
        paddingTop: '20%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        color: '#581400',
        fontSize: 24,
        fontWeight: 'bold',
    }


});


export default withNavigation(CommonBanner);
