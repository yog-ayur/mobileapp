import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    ScrollView,
    Dimensions,
    Animated
} from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;



export default class Carousal extends Component {

    scrollRef = React.createRef()
    state = {
        selectedIndex: 0,
        
    }

    componentDidMount = () => {
        setInterval(() => {
            this.setState((prev) => {
                return {
                    selectedIndex: prev.selectedIndex === this.props.images.length - 1 ? 0 : prev.selectedIndex + 1
                }
            }, () => {
                this.scrollRef.current.scrollTo({
                    animated: true,
                    y: 0,
                    x: (width*60/100) * this.state.selectedIndex,
                    duration:500
                })
            })
        }, 3000)
    }
    setSelectedIndex = (event) => {
        const viewSize = event.nativeEvent.layoutMeasurement.width;
        const contentOffset = event.nativeEvent.contentOffset.x;
        const selectedIndex = Math.floor(contentOffset / viewSize);
        console.log(viewSize, contentOffset)
        this.setState({ selectedIndex })
    }
    render() {
        const images = this.props.images
        return (
            <View style={styles.container}>
                <ScrollView
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}
                    onMomentumScrollEnd={this.setSelectedIndex}
                    bounces={false}
                    scrollEventThrottle={0}
                    ref={this.scrollRef}>
                    {images.map((image, i) => {
                        return (
                            <View key={i} style={{ marginLeft:10}}>
                                <Image source={image} style={styles.image} resizeMode='contain' />
                            </View>
                        )
                    }
                    )}
                </ScrollView>
                {this.props.text === 'carousal' && <View style={styles.circleContainer}>
                    {images.map((image, i) => {
                        return (
                            <View key={i} style={[styles.circle, { opacity: i === this.state.selectedIndex ? 0.5 : 1 }]}></View>
                        )
                    })}
                </View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display:'flex',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:14,
        paddingRight:14,
    },
    image: {
        width: width * 30 / 100,
        height: height * 15 / 100,
        borderRadius:7,
    },
  
    circleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        position: "absolute",
        width: '100%',
        paddingTop: height * 45 / 100
    },
    circle: {
        width: 4,
        height: 4,
        borderRadius: 2,
        backgroundColor: 'blue',
        margin: 4
    },
    activeCircle: {
        width: 4,
        height: 4,
        borderRadius: 2,
        backgroundColor: 'red',
        margin: 4
    }
});